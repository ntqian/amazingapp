package edu.wm.cs.cs301.nathan_qian.falstad;

import java.util.ArrayList;

import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;

/**
 * Encapsulates the draw functions. When a draw method is called, the shape and its
 * color is saved in the shapes array list. CanvasView will retrieve the shapes array
 * list and draw the corresponding shape with its color.
 * @author Nathan Qian, Helen Li
 *
 */
public class MazePanel{
	/* Panel operates a double buffer see
	 * http://www.codeproject.com/Articles/2136/Double-buffer-in-standard-Java-AWT
	 * for details
	 */
	
	private ArrayList<Tuple> shapes;

	/**
	 * Constructor
	 */
	public MazePanel(){
		shapes = new ArrayList<Tuple>();
	}
	
	/**
	 * Returns a single RGB value with the given values of red, green, and blue
	 * @param red
	 * @param green
	 * @param blue
	 * @return
	 */
	public static int getRGB(int red, int green, int blue){
		return Color.rgb(red, green, blue);
	}

	/**
	 * Places a request to draw a rectangle
	 * @param i: how far the left side of the rectangle is from the left 
	 * @param j: how far the top side of the rectangle is from the top
	 * @param viewWidth: how far the right side of the rectangle is from the left
	 * @param viewHeight: how far the bottom side of the rectangle is from the top
	 * @param color: color of the rectangle
	 */
	public void fillRect(int i, int j, int viewWidth, int viewHeight, int color) {
		Rect rect = new Rect(i, j, viewWidth, viewHeight);
		Paint paint = new Paint();
		paint.setColor(color);
		Tuple element = new Tuple(rect, paint);
		shapes.add(element);
	}

	/**
	 * Places a request to draw a polygon
	 * @param xps: array of the vertices' x coordinate
	 * @param yps: array of the vertices' y coordinate
	 * @param color: color of the polygon
	 */
	public void fillPolygon(int[] xps, int[] yps, int color) {
		Path path = new Path();
		path.moveTo(xps[0], yps[0]);
		for(int j = 0; j < xps.length; j++){
			path.lineTo(xps[j], yps[j]);
		}
		Paint paint = new Paint();
		paint.setColor(color);
		Tuple element = new Tuple(path, paint);
		shapes.add(element);
	}

	/**
	 * Places a request to draw a line
	 * @param nx1: starting x coordinate
	 * @param ny1: starting y coordinate
	 * @param nx2: ending x coordinate
	 * @param ny12: ending y coordinate
	 * @param color: color the line
	 */
	public void drawLine(int nx1, int ny1, int nx2, int ny12, int color) {
		int[] line = new int[4];
		line[0] = nx1;
		line[1] = ny1;
		line[2] = nx2;
		line[3] = ny12;
		Paint paint = new Paint();
		paint.setColor(color);
		Tuple element = new Tuple(line, paint);
		shapes.add(element);
	}

	/**
	 * Places a request to draw an oval. Uses rectangle parameters
	 * @param i: how far the left side of the rectangle is from the left
	 * @param j: how far the top side of the rectangle is from the top
	 * @param cirsiz: how far the right side of the rectangle is from the left
	 * @param cirsiz2: how far the bottom side of the rectangle is from the top
	 * @param color: color of the oval
	 */
	public void fillOval(int i, int j, int cirsiz, int cirsiz2, int color) {
		RectF oval = new RectF(i, j, cirsiz, cirsiz2);
		Paint paint = new Paint();
		paint.setColor(color);
		Tuple element = new Tuple(oval, paint);
		shapes.add(element);
	}	
	
	public ArrayList<Tuple> getShapes(){
		return shapes;
	}
}
