package edu.wm.cs.cs301.nathan_qian.ui;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import edu.wm.cs.cs301.nathan_qian.falstad.CollisionException;
import edu.wm.cs.cs301.nathan_qian.falstad.CustomRobot;
import edu.wm.cs.cs301.nathan_qian.falstad.DiscreteTremaux;
import edu.wm.cs.cs301.nathan_qian.falstad.Gambler;
import edu.wm.cs.cs301.nathan_qian.falstad.ManualDriver;
import edu.wm.cs.cs301.nathan_qian.falstad.Maze;
import edu.wm.cs.cs301.nathan_qian.falstad.OutOfEnergyException;
import edu.wm.cs.cs301.nathan_qian.falstad.Robot.Turn;
import edu.wm.cs.cs301.nathan_qian.falstad.TemplateDriver;
import edu.wm.cs.cs301.nathan_qian.falstad.UnsuitableRobotException;
import edu.wm.cs.cs301.nathan_qian.falstad.WallFollower;
import edu.wm.cs.cs301.nathan_qian.falstad.Wizard;

/**
 * Handles the navigation and graphics of the maze (to be implemented). The user can navigate
 * back to AMazeActivity or FinishAcivity.
 * @author Nathan Qian, Helen Li
 *
 */
public class PlayActivity extends Activity{

	private static final String PLAY_ACTIVITY = "PlayActivity";
	private Maze maze;
	private TemplateDriver driver;
	private CustomRobot robot;
	private boolean paused = true;
	private CanvasView graphics;
	private ProgressBar energyLevel;
	private TextView energyText;
	private Handler handler = new Handler();
	private MediaPlayer rightPlayer, wrongPlayer;
	private int[][] distance;

	@Override
	/**
	 * Generates the screen and reads in the intent from GeneratingActivity.
	 * The correct robot driver is instantiated based upon the intent. Initializes
	 * the energy bar.
	 */
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Intent intent = getIntent();
		Bundle bundle = intent.getExtras();
		driver = stringToRobotDriver(bundle.getString("driver"));
		Log.v(PLAY_ACTIVITY, "Received intent");

		setLayout();
		getActionBar().setDisplayHomeAsUpEnabled(true);
		graphics = (CanvasView) findViewById(R.id.graphics);
		energyLevel = (ProgressBar) findViewById(R.id.energyLevel);
		energyText = (TextView) findViewById(R.id.energy_label);
		wrongPlayer = MediaPlayer.create(this, R.raw.wrong_direction);
		rightPlayer = MediaPlayer.create(this, R.raw.new_right_direction);
		Log.v(PLAY_ACTIVITY, "onCreate");

		GlobalMaze globalMaze = (GlobalMaze) getApplicationContext();
		this.maze = globalMaze.getMaze();
		int[] position = maze.getCurrentPosition();
		int direction = maze.getDirectionInt();
		robot = new CustomRobot(position[0], position[1], direction, true, true, true, true, false, false);
		robot.setMaze(maze);
		try{
			driver.setRobot(robot);
		} catch(UnsuitableRobotException e){
			Log.v(PLAY_ACTIVITY, "Caught excpetion with robot setting");
		}
		driver.setDistance(maze.mazedists);
		driver.setDimensions(maze.mazew, maze.mazeh);
		driver.setMaze(maze);
		// integrate maze with UI
		energyText.setText("Energy level: " + (int)robot.getBatteryLevel());
		energyLevel.setMax((int) robot.getBatteryLevel());
		energyLevel.setProgress((int) robot.getBatteryLevel());
		
		distance = maze.mazedists.getDists();
//		robot.setBatteryLevel(10);
	}

	/**
	 * Ends the game. Called when the robot runs out of energy or if the robot
	 * reaches the exit.
	 */
	private void endGame(boolean success){
		Log.v(PLAY_ACTIVITY, "Creating intent");
		Intent intent = new Intent(this, FinishActivity.class);
		float energyConsumed = driver.getEnergyConsumption();
		int distanceTraveled = driver.getPathLength();
		intent.putExtra("energy", energyConsumed);
		intent.putExtra("distance", distanceTraveled);
		intent.putExtra("success", success);

		Log.v(PLAY_ACTIVITY, "Passing intent");
		startActivity(intent);
	}

	/**
	 * Helper method that parses the string into a RobotDriver.
	 * @param driverString: the string of desired RobotDriver
	 * @return returns the corresponding RobotDriver 
	 */
	private TemplateDriver stringToRobotDriver(String driverString){
		if(driverString.equals("Manual")){
			Log.v(PLAY_ACTIVITY, "Manual driver created");
			return new ManualDriver();
		}
		else if(driverString.equals("Gambler")){
			Log.v(PLAY_ACTIVITY, "Gambler driver created");
			return new Gambler();
		}
		else if(driverString.equals("Wall Follower")){
			Log.v(PLAY_ACTIVITY, "Wall Follower driver created");
			return new WallFollower();
		}
		else if(driverString.equals("Tremaux")){
			Log.v(PLAY_ACTIVITY, "Tremaux driver created");
			return new DiscreteTremaux();
		}
		else{
			Log.v(PLAY_ACTIVITY, "Wizard driver created");
			return new Wizard();
		}
	}

	public void rotateLeft(View view){
		Log.v(PLAY_ACTIVITY, "Turning left");
		try{
			driver.rotate(Turn.LEFT);
			updateEnergy();
			graphics.invalidate();
		} catch(OutOfEnergyException e){
			endGame(false);
		}
	}

	public void rotateRight(View view){
		Log.v(PLAY_ACTIVITY, "Turning right");
		try{
			driver.rotate(Turn.RIGHT);
			updateEnergy();
			graphics.invalidate();
		} catch(OutOfEnergyException e){
			endGame(false);
		}
	}

	public void moveForward(View view){
		Log.v(PLAY_ACTIVITY, "Moving forward");
		try{
			int oldx = robot.xPosition;
			int oldy = robot.yPosition;
			driver.move(1);
			updateEnergy();
			graphics.invalidate();
			if(gettingCloser(oldx, oldy)){
				rightPlayer.start();
			}
			else{
				wrongPlayer.start();
			}
		} catch(CollisionException e){
			Toast toast = Toast.makeText(this, "Can't drive into walls", Toast.LENGTH_SHORT);
			toast.show();
		} catch(OutOfEnergyException e){
			endGame(false);
		} catch(Exception e){
			Log.v(PLAY_ACTIVITY, "Must move a distance greater than zero");
		}
		if(driver.robot.isAtGoal()){
			endGame(true);
		}
	}

	public void togglePlayPause(View view){
		paused = !paused;
		if(!paused){
			handler.postDelayed(automatedExplorer, 1500);
		}
		else{
			handler.removeCallbacks(automatedExplorer);
		}
	}

	/**
	 * Toggles the maze. 
	 * @param view: the toggle maze toggle button
	 */
	public void toggleMaze(View view){
		maze.toggleMapMode();
		graphics.invalidate();
		maze.notifyViewerRedraw();
	}

	/**
	 * Toggles the solution. Currently, it transitions from PlayActivity
	 * to FinishActivity by calling endGame()
	 * @param view: the toggle solution toggle button
	 */
	public void toggleSolution(View view){
		maze.toggleShowSolution();
		graphics.invalidate();
		maze.notifyViewerRedraw();
	}

	/**
	 * Toggles the walls.
	 * @param view: the toggle walls toggle button
	 */
	public void toggleWalls(View view){
		maze.toggleShowMaze();
		graphics.invalidate();
		maze.notifyViewerRedraw();
	}

	/**
	 * Enlarges the map when the map is displayed.
	 * @param view
	 */
	public void incrementMapScale(View view){
		graphics.invalidate();
		maze.notifyViewerIncrementMapScale();
	}

	/**
	 * Shrinks the map when the map is displayed.
	 * @param view
	 */
	public void decrementMapScale(View view){
		graphics.invalidate();
		maze.notifyViewerDecrementMapScale();
	}

	private void setLayout(){
		if(driver instanceof ManualDriver){
			setContentView(R.layout.play_layout_manual);
		}
		else{
			setContentView(R.layout.play_layout_auotmated);
		}
	}

	private void updateEnergy(){
		energyText.setText("Energy: " + Float.toString(robot.getBatteryLevel()));
		energyLevel.setProgress((int) robot.getBatteryLevel());
	}

	private Runnable automatedExplorer = new Runnable(){
		public void run(){
			try{
				if(driver.robot.isAtGoal()){
					endGame(true);
					handler.removeCallbacks(this);
				}
				else{
					driver.drive2ExitOneStep();
					updateEnergy();
					graphics.invalidate();
					handler.postDelayed(this, 1500);
				}
			} catch(OutOfEnergyException e){
				endGame(false);
			} catch(ArrayIndexOutOfBoundsException e){
				Log.v(PLAY_ACTIVITY, "Robot is outside of the maze");
			}
			catch(Exception e){
				Log.v(PLAY_ACTIVITY, "Null pointer exception");
			}
		}
	};
	
	private boolean gettingCloser(int oldx, int oldy){
		if(distance[oldx][oldy] < distance[robot.xPosition][robot.yPosition]){
			return false;
		}
		else{
			return true;
		}
	}
}
