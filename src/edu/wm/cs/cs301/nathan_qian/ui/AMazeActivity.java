package edu.wm.cs.cs301.nathan_qian.ui;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

/**
 * Class that handles the starting screen. The users selects which maze generating algorithm,
 * robot driver, and size of the maze on this screen. Passes all of this data to
 * GeneratingActivity to instantiate the corresponding Maze object.
 * @author Nathan Qian, Helen Li
 *
 */
public class AMazeActivity extends Activity {

	private static final String A_MAZE_ACTIVITY = "AMazeActivity";
	private Spinner mazeSpinner;
	private Spinner driverSpinner;
	private SeekBar skillBar;
	private TextView skillText;
	private int skill;
	private boolean save = false;

	@Override
	/**
	 * Generates all the items onto the screen. Primarily populates the spinners to display the options for
	 * the maze.
	 */
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.a_maze_layout);
		Log.v(A_MAZE_ACTIVITY, "onCreate()");

		mazeSpinner = (Spinner) findViewById(R.id.maze_spinner);
		ArrayAdapter<CharSequence> mazeAdapter = ArrayAdapter.createFromResource(this,
				R.array.maze_array, android.R.layout.simple_spinner_dropdown_item);
		mazeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mazeSpinner.setAdapter(mazeAdapter);

		driverSpinner = (Spinner) findViewById(R.id.driver_spinner);
		ArrayAdapter<CharSequence> driverAdapter = ArrayAdapter.createFromResource(this, R.array.driver_array, android.R.layout.simple_spinner_dropdown_item);
		driverAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		driverSpinner.setAdapter(driverAdapter);

		skillBar = (SeekBar) findViewById(R.id.seekBar1);
		skillText = (TextView) findViewById(R.id.skill_label);
		skillBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				skillText.setText(String.valueOf(progress));
				skill = progress;
			}
		});
	}


	@Override
	/**
	 * Creates an action bar for AMazeActivity. The action bar has a start button that will transition the activity
	 * to GeneratingActivity.
	 */
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.start, menu);
		Log.v(A_MAZE_ACTIVITY, "onCreateOptionsMenu()");
		return true;
	}

	@Override
	/**
	 * When the start button is pressed, creates the intent that will be passed on to GeneratingActivity. 
	 * GeneratingActivity will read in the strings and instantiate the corresponding Maze object.
	 */
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.start) {
			String mazeAlgorithm = mazeSpinner.getSelectedItem().toString();
			Log.v(A_MAZE_ACTIVITY, "Maze algorithm: " + mazeAlgorithm);
			String robotDriver = driverSpinner.getSelectedItem().toString();
			Log.v(A_MAZE_ACTIVITY, "RobotDriver: " + robotDriver);
			Log.v(A_MAZE_ACTIVITY, "Skill: " + skill);
			
			CheckBox checkBox = (CheckBox) findViewById(R.id.save_box);
			if(checkBox.isChecked()){
				save = true;
			}

			Intent intent = new Intent(this, GeneratingActivity.class);
			intent.putExtra("maze", mazeAlgorithm);
			intent.putExtra("driver", robotDriver);
			intent.putExtra("skill", skill);
			intent.putExtra("save", save);
			startActivity(intent);
		}
		return super.onOptionsItemSelected(item);
	}
}
