package edu.wm.cs.cs301.nathan_qian.ui;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import edu.wm.cs.cs301.nathan_qian.falstad.Maze;
import edu.wm.cs.cs301.nathan_qian.falstad.Tuple;

public class CanvasView extends View {

	private static final String CANVAS_VIEW = "CanvasView";
	private Maze maze;
	private ArrayList<Tuple> shapes;
	
	
	/**
	 * Constructor
	 * @param context
	 * @param attrs
	 */
	public CanvasView(Context context, AttributeSet attrs) {
		super(context, attrs);
		Log.v(CANVAS_VIEW, "Instantiating CanvasView");
		init(context);
	}

	/**
	 * Retrieves the maze from the shared address space to get maze's MazePanel
	 * attribute and draw the shapes saved in MazePanel
	 * @param context
	 */
	private void init(Context context){
		GlobalMaze globalMaze = (GlobalMaze) context.getApplicationContext();
		Log.v(CANVAS_VIEW, "Retrieved singleton");
		this.maze = globalMaze.getMaze();
		Log.v(CANVAS_VIEW, Integer.toString(maze.getWidth()));
		Log.v(CANVAS_VIEW, "Retrieved maze");
		shapes = maze.panel.getShapes();
		Log.v(CANVAS_VIEW, "Retrieved shapes");
	}
	
	/**
	 * Accesses MazePanel's shapes array list and draws the corresponding
	 * shape and paint.
	 */
	protected void onDraw(Canvas canvas){
		super.onDraw(canvas);
		Log.v(CANVAS_VIEW, "onDraw called");
		for(int i = 0; i < shapes.size(); i++){
			if(shapes.get(i).shape instanceof Rect){
				Rect rect = (Rect) shapes.get(i).shape;
				Paint paint = shapes.get(i).paint;
				canvas.drawRect(rect, paint);
			}
			else if(shapes.get(i).shape instanceof Path){
				Path path = (Path) shapes.get(i).shape;
				Paint paint = shapes.get(i).paint;
				canvas.drawPath(path, paint);
			}
			else if(shapes.get(i).shape instanceof int[]){
				int[] line = (int[]) shapes.get(i).shape;
				Paint paint = shapes.get(i).paint;
				canvas.drawLine(line[0], line[1], line[2], line[3], paint);
			}
			else if(shapes.get(i).shape instanceof RectF){
				RectF rectF = (RectF) shapes.get(i).shape;
				Paint paint = shapes.get(i).paint;
				canvas.drawOval(rectF, paint);
			}
		}
		shapes.clear();
	}
}
