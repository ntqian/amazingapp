package edu.wm.cs.cs301.nathan_qian.ui;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

/**
 * Handles the end of the maze. 
 * @author Nathan Qian, Helen Li
 *
 */
public class FinishActivity extends Activity{
	
	private static final String FINISH_ACTIVITY = "FinishActivity";
	private float energyConsumed;
	private int distanceTraveled;
	private boolean success;
	private MediaPlayer winPlayer, losePlayer;

	@Override
	/**
	 * Generates the screen. Reads in the intent from PlayActivity and displays
	 * energy consumed and distance traveled on to the screen.
	 */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.finish_layout);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        winPlayer = MediaPlayer.create(this, R.raw.win_sound);
        losePlayer = MediaPlayer.create(this, R.raw.lose_sound);
        
        Intent intent = getIntent();
        energyConsumed = intent.getFloatExtra("energy", 0);
        distanceTraveled = intent.getIntExtra("distance", 0);
        success = intent.getBooleanExtra("success", true);
        Log.v(FINISH_ACTIVITY, "Received intent");
        
        TextView textView1 = (TextView) findViewById(R.id.success_text);
        textView1.setText(getWinLoseMessage());
        
        TextView textView2 = (TextView) findViewById(R.id.energy_text);
        textView2.setText("Energy consumed: " + energyConsumed);
        
        TextView textView3 = (TextView) findViewById(R.id.distance_text);
        textView3.setText("Distance traveled: " + distanceTraveled);
        
        if(success){
        	winPlayer.start();
        }
        else{
        	losePlayer.start();
        }
	}
	
	@Override
	/**
	 * Returns to AMazeActivity if the user presses the up button.
	 */
	public boolean onOptionsItemSelected(MenuItem item){
		switch (item.getItemId()){
		case android.R.id.home:
			Log.v(FINISH_ACTIVITY, "Returning back to AMazeActivity");
			winPlayer.stop();
			losePlayer.stop();
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private String getWinLoseMessage(){
		if(success){
			return "Game over! You successfully navigated out of the maze.";
		}
		else{
			return "Game over! You ran out of energy.";
		}
	}
}
