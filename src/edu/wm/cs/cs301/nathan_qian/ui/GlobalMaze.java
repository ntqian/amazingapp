package edu.wm.cs.cs301.nathan_qian.ui;

import android.app.Application;
import edu.wm.cs.cs301.nathan_qian.falstad.Maze;
import edu.wm.cs.cs301.nathan_qian.falstad.MazePanel;

/**
 * Class that stores the maze in a shared address space.
 * @author Nathan Qian, Helen Li
 *
 */
public class GlobalMaze extends Application {

	public Maze maze;
	public int size;
	
	public Maze getMaze(){
		return maze;
	}
	
	public void setMaze(Maze maze){
		this.maze = maze;
	}
	
	public void setMazePanel(MazePanel panel){
		maze.panel = panel;
	}
	
	public void setSize(int size){
		this.size = size;
	}
	
	public int getSize(){
		return size;
	}
}
