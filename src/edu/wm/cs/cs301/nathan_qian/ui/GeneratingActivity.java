package edu.wm.cs.cs301.nathan_qian.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Random;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import edu.wm.cs.cs301.nathan_qian.falstad.Distance;
import edu.wm.cs.cs301.nathan_qian.falstad.Maze;
import edu.wm.cs.cs301.nathan_qian.falstad.MazeFileReader;
import edu.wm.cs.cs301.nathan_qian.falstad.MazeFileWriter;

/**
 * Handles the creation of the maze. Displays the progress of the maze construction (to be implemented)
 * and passes the created maze on to PlayActivity
 * @author Nathan Qian, Helen Li
 *
 */
public class GeneratingActivity extends Activity{

	private static final String GENERATING_ACTIVITY = "GeneratingActivity";

	private Maze maze;
	private boolean mazeBuilt = false;
	private Intent intent;
	private int progress = 0;
	private ProgressBar progressBar;

	@Override
	/**
	 * Generates the screen. Reads in the intent from AMazeActivity and parses the data into the
	 * appropriate maze. As soon as it's done, it passes on the maze object and robot driver 
	 * information on to PlayActivity.
	 */
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.generating_layout);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		progressBar = (ProgressBar) findViewById(R.id.progressBar1);
		progressBar.setMax(100);

		GlobalMaze globalMaze = (GlobalMaze)getApplicationContext();

		Log.v(GENERATING_ACTIVITY, "Starting GeneratingActivity");
		Intent intent = getIntent();
		String mazeString = intent.getStringExtra("maze");
		String driverString = intent.getStringExtra("driver");
		Boolean save = intent.getBooleanExtra("save", false);
		int skill = intent.getIntExtra("skill", 0);
		Log.v(GENERATING_ACTIVITY, "Recieved intent");
		
		Random random = new Random();
		int seed = random.nextInt(1337);

		maze = null;
		if(mazeString.equals("Backtracking")){
			maze = new Maze(0, skill, seed);
		}
		else if(mazeString.equals("Prim")){
			maze = new Maze(1, skill);
		}
		else if(mazeString.equals("Aldous Broder")){
			maze = new Maze(2, skill);
		}
		else{
			int[] settings = loadMaze(mazeString);
			skill = settings[0];
			seed = settings[1];
			maze = new Maze(0, skill, seed);
		}
		maze.init();
		Log.v(GENERATING_ACTIVITY, "Initializing maze");

		maze.build();
		final Handler hanler = new Handler();
		new Thread(maze.mazebuilder){
			public void run(){
				//					maze.build();
				while (progress < 100){
					progress++;

					hanler.post(new Runnable(){
						public void run(){
							progressBar.setProgress(progress);
						}
					});
				}
			}
		}.start();


		if(save){
			Log.v(GENERATING_ACTIVITY, "Saving maze");
//			MazeBuilder b = maze.mazebuilder;
//			byte[] bytes = MazeFileWriter.store("Maze " + skill, b.width, b.height, b.rooms, b.expectedPartiters, b.root, b.cells, b.dists.getDists(), b.startx, b.starty);
//			try{
//				FileOutputStream outputStream = openFileOutput("Maze " + skill, Context.MODE_PRIVATE);
//				outputStream.write(bytes);
//				outputStream.close();
//				Log.v(GENERATING_ACTIVITY, "Saved maze in " + getFilesDir());
//			} catch(Exception e){
//				Log.v(GENERATING_ACTIVITY, "Error in saving maze");
//			}
			
			SharedPreferences pref = getSharedPreferences("Maze " + skill, Context.MODE_PRIVATE);
			SharedPreferences.Editor editor = pref.edit();
			editor.putInt("skill", skill);
			editor.putInt("seed", seed);
			editor.commit();
		}

		try{
			maze.getMazeBuilder().buildThread.join();
			Log.v(GENERATING_ACTIVITY, "Building maze");
			globalMaze.setMaze(maze);
			globalMaze.setSize(skill);

			mazeBuilt = true;

			this.intent = new Intent(this, PlayActivity.class);
			Bundle bundle = new Bundle();
			bundle.putString("driver", driverString);
			this.intent.putExtras(bundle);
			Log.v(GENERATING_ACTIVITY, "Finished creating maze and intent");
		} catch(InterruptedException e){
			Log.v(GENERATING_ACTIVITY, "Maze building interrupted");
		}
	}

	@Override
	/**
	 * Returns to AMazeActivity if the user presses the up button.
	 */
	public boolean onOptionsItemSelected(MenuItem item){
		switch (item.getItemId()){
		case android.R.id.home:
			Log.v(GENERATING_ACTIVITY, "Returning back to AMazeActivity");
			maze.getMazeBuilder().buildThread.interrupt();
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * Button method that starts the maze after the maze is done constructing
	 * @param view
	 */
	public void startPlayActivity(View view){
		if(mazeBuilt == true){
			Log.v(GENERATING_ACTIVITY, "Passing intent to PlayActivity");
			startActivity(intent);
		}
	}

	private void loadMaze(String mazeString, int asdf){
		File directory = getFilesDir();
		String filePath = directory.getPath();
		Log.v(GENERATING_ACTIVITY, "File path: " + filePath);

		try {
			FileInputStream fis = openFileInput(mazeString);

			MazeFileReader reader = new MazeFileReader(filePath + mazeString + ".xml");
			maze.mazeh = reader.getHeight() ;
			maze.mazew = reader.getWidth() ;
			Distance d = new Distance(reader.getDistances()) ;
			maze.newMaze(reader.getRootNode(),reader.getCells(),d,reader.getStartX(), reader.getStartY()) ;
		} catch (FileNotFoundException e) {
			Log.v(GENERATING_ACTIVITY, "Error in loading maze");
			NavUtils.navigateUpFromSameTask(this);
		}
	}

	private int[] loadMaze(String mazeString){
		Log.v(GENERATING_ACTIVITY, "Loading maze");
		SharedPreferences prefs = getSharedPreferences(mazeString, Context.MODE_PRIVATE);
		int seed = prefs.getInt("seed", 1337);
		int skill = prefs.getInt("skill", 0);
		int[] ret = {skill, seed};
		return ret;
	}
}
