package edu.wm.cs.cs301.nathan_qian.falstad;

import java.util.ArrayList;
import java.util.HashMap;
import edu.wm.cs.cs301.nathan_qian.falstad.Robot.Direction;
import edu.wm.cs.cs301.nathan_qian.falstad.Robot.Turn;

public class DiscreteTremaux extends TemplateDriver {

	protected SingleRandom random;
	HashMap<String, ArrayList<Integer>> paths = new HashMap<String, ArrayList<Integer>>();
	HashMap<String, Integer> parent = new HashMap<String, Integer>();
	protected boolean starting;
	protected boolean backtracking;
	
	public DiscreteTremaux(){
		energyConsumed = 0;
		distanceTraveled = 0;
		width = 0;
		height = 0;
		distance = null;
		robot = null;
		SingleRandom.setSeed(6942);
		random = SingleRandom.getRandom();
		starting = true;
		backtracking = false;
	}
	
	@Override
	public void setRobot(Robot r) throws UnsuitableRobotException {
		if(!r.equals(null) && r.hasJunctionSensor() && r.hasDistanceSensor(Direction.FORWARD) 
				&& r.hasDistanceSensor(Direction.LEFT) && !r.hasDistanceSensor(Direction.RIGHT) 
				&& !r.hasDistanceSensor(Direction.BACKWARD) && r.hasRoomSensor()){
			robot = (TemplateRobot) r;
		}
		else{
			throw new UnsuitableRobotException();
		}
	}
	
	private String coordinateToString(){
		return "(" + robot.xPosition + "," + robot.yPosition + ")";
	}
		
	private void turnToParent() throws OutOfEnergyException{
		String key = coordinateToString();
		if(robot.dir - parent.get(key) == 1 || robot.dir - parent.get(key) == -3){
			rotate(Turn.RIGHT);
		}
		else if(robot.dir - parent.get(key) == -1 || robot.dir - parent.get(key) == 3){
			rotate(Turn.LEFT);
		}
		else if(Math.abs(robot.dir - parent.get(key)) == 2){
			rotate(Turn.AROUND);
		}
	}
	
	// TODO: check all possible move conditions
	private void advance() throws OutOfEnergyException, CollisionException, Exception{
		if(robot.isInsideRoom() && distanceToObstacle(Direction.FORWARD) > 0){
			move(1);
		}
		else if(robot.isInsideRoom() && distanceToObstacle(Direction.FORWARD) == 0 
				&& distanceToObstacle(Direction.LEFT) == 0){
			rotate(Turn.RIGHT);
		}
		else if(robot.isInsideRoom() && distanceToObstacle(Direction.FORWARD) > 0
				&& distanceToObstacle(Direction.LEFT) > 0){
			setParent();
			ArrayList<Integer> pathList = createPathList();
			String position = coordinateToString();
			paths.put(position, pathList);
			rotate(Turn.LEFT);
			move(1);
		}
		else if(distanceToObstacle(Direction.FORWARD) > 0){
			move(1);
		}
		else if(distanceToObstacle(Direction.RIGHT) > 0 && !robot.isAtJunction()){
			rotate(Turn.RIGHT);
		}
		else if(distanceToObstacle(Direction.LEFT) > 0 && !robot.isAtJunction()){
			rotate(Turn.LEFT);
		}
		else{
			rotate(Turn.AROUND);
		}
	}
	
	private void choosePath(ArrayList<Integer> pathList) throws OutOfEnergyException{
		int index = random.nextIntWithinInterval(0, pathList.size()-1);
		int direction = pathList.get(index);
		pathList.remove(index);
		if((robot.dir + 1)%4 == direction){
			rotate(Turn.LEFT);
		}
		else if((robot.dir -1)%4 == direction){
			rotate(Turn.RIGHT);
		}
	}
	
	private ArrayList<Integer> createPathList() throws OutOfEnergyException{
		ArrayList<Integer> ret = new ArrayList<Integer>();
		int direction = robot.dir;
		if(distanceToObstacle(Direction.LEFT) > 0){
			ret.add((direction+1)%1);
		}
		if(distanceToObstacle(Direction.FORWARD) > 0){
			ret.add(direction);
		}
		if(distanceToObstacle(Direction.RIGHT) > 0){
			ret.add((direction+3)%4);
		}
		return ret;
	}
	
	private boolean isAtNewJunction(){
		String position = coordinateToString();
		ArrayList<Integer> result = paths.get(position);
		if(result.equals(null)){
			return true;
		}
		else{
			return false;
		}
	}
	
	private void setParent(){
		String position = coordinateToString();
		parent.put(position, robot.dir);
	}
	
	@Override
	// TODO: check all possible cases (setting parent when first starting)
	public boolean drive2ExitOneStep() throws OutOfEnergyException, Exception {
		if(starting || !robot.isAtJunction()){
			advance();
		}
		else if(robot.isAtJunction() && isAtNewJunction()){
			if(starting){
				starting = false;
			}
			setParent();
			ArrayList<Integer> pathList = createPathList();
			String position = coordinateToString();
			paths.put(position, pathList);
		}
		else if(!isAtNewJunction()){
			String position = coordinateToString();
			ArrayList<Integer> pathList = paths.get(position);
			if(pathList.size() > 0){
				choosePath(pathList);
				advance();
			}
			else{
				turnToParent();
				advance();
			}
		}
		return true;
	}
}
