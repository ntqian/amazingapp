package edu.wm.cs.cs301.nathan_qian.falstad;
import java.util.ArrayList;
import java.util.HashMap;
public class MazeBuilderAldousBroder extends MazeBuilder{
	protected int rooms;

	/**
	 * Constructor
	 */
	public MazeBuilderAldousBroder(){
		SingleRandom.setSeed(89273);
		random = SingleRandom.getRandom();
	}

	public MazeBuilderAldousBroder(boolean deterministic){
		super(deterministic);
	}

	/**Initializes attributes of the MazeBuilderAldousBroder. Creates the thread that will
	 * create the maze when run() is called.
	 * @param mz: Maze object that represents the maze's state
	 * @param w: width of the maze
	 * @param h: height of the maze
	 * @param roomct: the number of rooms
	 * @param pc: must be greater than zero
	 */
	public void build(Maze mz, int w, int h, int roomct, int pc){
		init(mz, w, h, roomct, pc);
		buildThread = new Thread(this);
		buildThread.start();
	}

	/**
	 * Helper method that initializes all the of the attributes. Checks if the parameters
	 * are valid.
	 * @param mz: Maze object that represents the maze's state
	 * @param w: width of the maze
	 * @param h: height of the maze
	 * @param roomct: number of rooms
	 * @param pc: must be greater than zero
	 */
	public boolean init(Maze mz, int w, int h, int roomct, int pc){
		// A worthwhile maze to solve must have a width or height of two
		// A maze with with width or height of one is just a hallway
		if(w < 2){
			//System.out.println(false);
			return false;
		}
		else if( h < 2){
			//System.out.println(false);
			return false;
		}
		else if(roomct < 0){
			//System.out.println(false);
			return false;
		}
		else if(pc < 1){
			//System.out.println(false);
			return false;
		}
		maze = mz;
		width = w;
		height = h;
		rooms = roomct;
		expectedPartiters = pc;
		cells = new Cells(w,h) ;
		dists = new Distance(w,h) ;
		//System.out.println(true);
		return true;
	}

	/**
	 * The Aldous Broder algorithm. Starts at a random cell and selects a random neighbor. If the neighbor has
	 * never been visited before, delete the wall between the neighbor and the cell. Continue until all cells of
	 * the maze has been visited.
	 */
	protected void generate(){
		// Pick a random cell
		int x = random.nextIntWithinInterval(0, width-1);
		int y = random.nextIntWithinInterval(0, height-1);
		cells.setCellAsVisited(x, y);
		// dictionary will store which cells have been visited
		HashMap<String, Boolean> dictionary = new HashMap<String, Boolean>();
		int total = height * width;	
		// counts the number of rooms visited
		int counter = 0;
		dictionary.put("" + x + "," + y, true);
		// While the number of cells visited is less than the total cells
		while(counter < total -1){
			// generates a random direction
			int dir[] = generateValidCoordinate(x, y);
			// retrieves the corresponding dx and dy from direction
			int dx = dir[0];			
			int dy = dir[1];			
			int tempx = dx + x;
			int tempy = dy + y;
			// If the neighbor hasn't been visited, delete the walls and 
			// add the neighbor to the visted dictionary
			if(dictionary.containsKey("" + tempx + "," + tempy) == false){
				cells.deleteWall(x, y, dx, dy);
				dictionary.put("" + tempx + "," + tempy, true);
				counter++;
				cells.setCellAsVisited(tempx, tempy);
			}
			// The neighboring cell is now the current cell
			x = tempx;
			y = tempy;
		}
		// Designates the exit
		final int[] remote = dists.computeDistances(cells);
		cells.setExitPosition(remote[0], remote[1]);
		//pseduo code beins here lol makeExit();

		// neighbor = pepe.getNeighbor
		// if neighbor has not been visited
		// pepe.deleteWall
		// visitedCount++
		// add neighbor to has been visited
		// pepe = neighbor
	}

	/**
	 * Method generates a random valid value for dx and dy for a given cell.
	 * @param variable X or Y
	 * @return new value of X or Y
	 **/
	public int[] generateValidCoordinate(int x, int y){
		ArrayList<Integer> list = new ArrayList<Integer>();
		if(x < width-1){
			list.add(0);
		}
		if(y < height-1){
			list.add(1);
		}
		if(x > 0){
			list.add(2);
		}
		if(y > 0){
			list.add(3);
		}
		int index = random.nextIntWithinInterval(0, list.size()-1);
		int[] array = new int[2];
		int dx = Constants.DIRS_X[list.get(index)];
		int dy = Constants.DIRS_Y[list.get(index)];
		array[0] = dx;
		array[1] = dy;
		return array;
	}

	/**
	 * Creates the maze using the Aldous Broder algorithm.
	 */
	public void run(){
		try{
			cells.initialize();
			generateRooms();

			Thread.sleep(SLEEP_INTERVAL);

			generate();

			Thread.sleep(SLEEP_INTERVAL);

			final int colchange = random.nextIntWithinInterval(0, 255); // used in the constructor for Segments  class Seg
			final BSPBuilder b = new BSPBuilder(maze, dists, cells, width, height, colchange, expectedPartiters) ;
			BSPNode root = b.generateBSPNodes();

			Thread.sleep(SLEEP_INTERVAL);

			maze.newMaze(root, cells, dists, startx, starty);
		}
		catch(InterruptedException ex){
			dbg("Catching signal to stop");
		}
	}

	/**
	 * Generate all rooms in a given maze where initially all walls are up. Rooms are placed randomly and of random sizes
	 * such that the maze can turn out to be too small to accommodate the requested number of rooms (class attribute rooms). 
	 * In that case less rooms are produced.
	 * @return generated number of rooms
	 */
	private int generateRooms() {
		// Rooms are randomly positioned such that it may be impossible to place the all rooms if the maze is too small
		// to prevent an infinite loop we limit the number of failed to MAX_TRIES == 250
		int tries = 0 ;
		int result = 0 ;
		while (tries < MAX_TRIES && result <= rooms) {
			if (placeRoom())
				result++ ;
			else
				tries++ ;
		}
		return result ;
	}

	/**
	 * Allocates space for a room of random dimensions in the maze.
	 * The position of the room is chosen randomly. The method is not sophisticated 
	 * such that the attempt may fail even if the maze has ample space to accommodate 
	 * a room of the chosen size. 
	 * @return true if room is successfully placed, false otherwise
	 */
	private boolean placeRoom() {
		// get width and height of random size that are not too large
		// if too large return as a failed attempt
		final int rw = random.nextIntWithinInterval(MIN_ROOM_DIMENSION, MAX_ROOM_DIMENSION);
		if (rw >= width-4)
			return false;

		final int rh = random.nextIntWithinInterval(MIN_ROOM_DIMENSION, MAX_ROOM_DIMENSION);
		if (rh >= height-4)
			return false;

		// proceed for a given width and height
		// obtain a random position (rx,ry) such that room is located on as a rectangle with (rx,ry) and (rxl,ryl) as corner points
		// upper bound is chosen such that width and height of room fits maze area.
		final int rx = random.nextIntWithinInterval(1, width-rw-1);
		final int ry = random.nextIntWithinInterval(1, height-rh-1);
		final int rxl = rx+rw-1;
		final int ryl = ry+rh-1;
		// check all cells in this area if they already belong to a room
		// if this is the case, return false for a failed attempt
		if (cells.areaOverlapsWithRoom(rx, ry, rxl, ryl))
			return false ;
		// since the area is available, mark it for this room and remove all walls
		// from this on it is clear that we can place the room on the maze
		cells.markAreaAsRoom(rw, rh, rx, ry, rxl, ryl); 
		return true;
	}
}
