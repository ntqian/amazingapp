package edu.wm.cs.cs301.nathan_qian.falstad;

import android.util.Log;

// interacts with the UI
// Run MazeApplication.java to execute ManualDriver
public class ManualDriver extends TemplateDriver{

	public ManualDriver(){
		energyConsumed = 0;
		distanceTraveled = 0;
		robot = null;
		width = 0;
		height = 0;
	}

	public void setRobot(Robot r) throws UnsuitableRobotException {
		Log.v("ManualDriver", "ManualDriver's setRobot()");
		if(!r.equals(null)){
			robot = (TemplateRobot) r;
		}
		else{
			throw new UnsuitableRobotException();
		}
		// if Robot r meets the minimal requirements for the ManualDriver
		// set basicRobot to r
		// else
		// throw UnsuitableRobotException
	}

	@Override
	public void setDimensions(int width, int height) {
		// give the driver information on the maze's dimension
		this.width = width;
		this.height = height;
	}

	@Override
	public void setDistance(Distance distance) {
		this.distance = distance;
	}

	@Override
	/**
	 * Solving algorithm. Uses BasicRobot.maze.mazedists to find a path to the exit. Robot.move() and Robot.rotate() 
	 * simulate key presses by calling maze.keyDown(). Should never be called since a user is operating the ManualDriver.
	 */
	public boolean drive2Exit() throws Exception {
		// TODO Auto-generated method stub
		// Key codes
		final int LEFT = 72;
		final int RIGHT = 76;
		final int FORWARD = 75;
		int[][] distances = ((BasicRobot) robot).maze.mazedists.getDists();
		int[] position = robot.getCurrentPosition();
		int xPosition = position[0];
		int yPosition = position[1];
		// while celldistance != 1 
		while(distances[xPosition][yPosition] != 1){
			// check where the borders are - if no border, add them to an array
			// for cells in array, check distances
			// go for the first one that is current distance-1
			int neighborDirection = getOneLessDistance(xPosition, yPosition, distances);
			if(robot.getBatteryLevel() < 3){
				throw new Exception();
			}
			else if(neighborDirection == (((BasicRobot)robot).dir+1) % 4){
				robot.rotate(Robot.Turn.LEFT);
				energyConsumed += robot.getEnergyForFullRotation()/4;
				//				((BasicRobot)robot).maze.keyDown(null, LEFT);

			}
			else if(neighborDirection == (((BasicRobot)robot).dir+3) % 4){
				robot.rotate(Robot.Turn.RIGHT);
				energyConsumed += robot.getEnergyForFullRotation()/4;
				//				((BasicRobot)robot).maze.keyDown(null, RIGHT);
			}
			if(robot.getBatteryLevel() < 6){
				throw new Exception();
			}
			else if(neighborDirection == (((BasicRobot)robot).dir+2) % 4){
				robot.rotate(Robot.Turn.AROUND);
				energyConsumed += robot.getEnergyForFullRotation()/2;
				//				((BasicRobot)robot).maze.keyDown(null, LEFT);
				//				((BasicRobot)robot).maze.keyDown(null, LEFT);
			}
			if(robot.getBatteryLevel() < 5){
				throw new Exception();
			}
			robot.move(1);
			energyConsumed += robot.getEnergyForStepForward();
			//			((BasicRobot)robot).maze.keyDown(null, FORWARD);
			position = robot.getCurrentPosition();
			xPosition = position[0];
			yPosition = position[1];
		}
		return false;
	}

	/**
	 * Finds the direction of the neighboring cell that has a distance of one less than the robot's current cell
	 * @param xPosition
	 * @param yPosition
	 * @param distances
	 * @return neighbor's cell direction relative to robot's current cell
	 */
	private int getOneLessDistance(int xPosition, int yPosition, int[][] distances) {
		return 0;
	}

	@Override
	public float getEnergyConsumption() {
		return energyConsumed;
	}

	@Override
	public int getPathLength() {
		return distanceTraveled;
	}
}
