package edu.wm.cs.cs301.nathan_qian.falstad;

import android.util.Log;
import edu.wm.cs.cs301.nathan_qian.falstad.Robot.Direction;
import edu.wm.cs.cs301.nathan_qian.falstad.Robot.Turn;

/**
 * Uses template design pattern. Defines common driver behavior
 * @author Nathan Qian, Helen Li
 *
 */
public abstract class TemplateDriver implements RobotDriver {

	private static final String TEMPLATE_ROBOT = "TemplateRobot";
	protected float energyConsumed;
	protected int distanceTraveled;
	protected int width;
	protected int height;
	protected Distance distance;
	protected Maze maze;
	public TemplateRobot robot;

	final static int LEFT = 72;
	final static int RIGHT = 76;
	final static int FORWARD = 75;

	public TemplateDriver(){
		energyConsumed = 0;
		distanceTraveled = 0;
	}

	@Override
	/**
	 * Each driver defines which robot to accept
	 */
	public void setRobot(Robot r) throws UnsuitableRobotException {
		Log.v(TEMPLATE_ROBOT, "TemplateRobot's setRobot");
		if(!r.equals(null) && r.hasJunctionSensor() && r.hasDistanceSensor(Direction.FORWARD) 
				&& r.hasDistanceSensor(Direction.LEFT) && !r.hasDistanceSensor(Direction.RIGHT) 
				&& !r.hasDistanceSensor(Direction.BACKWARD) && r.hasRoomSensor()){
			robot = (TemplateRobot) r;
		}
		else{
			throw new UnsuitableRobotException();
		}
	}

	@Override
	public void setDimensions(int width, int height) {
		this.width = width;
		this.height = height;
	}

	@Override
	public void setDistance(Distance distance) {
		this.distance = distance;
	}

	@Override
	/**
	 * Each driver defines how to drive to exit
	 */
	public boolean drive2Exit() throws Exception {
		return false;
	}

	@Override
	public float getEnergyConsumption() {
		return energyConsumed;
	}

	@Override
	public int getPathLength() {
		return distanceTraveled;
	}

	/**
	 * Encapsulates the robot's movement and driver's energy consumption and distance traveled
	 * @param distance: number of cells the robot should move forward
	 * @throws Exception: throws Exception if the robot doesn't have enough energy to move
	 */
	public void move(int distance) throws Exception, OutOfEnergyException, CollisionException{
		robot.move(distance);
		for(int i = 0; i < distance; i++){
			maze.walk(1);
		}
		energyConsumed = energyConsumed + ((robot.getEnergyForStepForward()+1)*distance);
		distanceTraveled = distanceTraveled + distance;
	}

	/**
	 * Encapsulates the robot's movement and the driver's energy consumption
	 * @param direction: direction the robot should rotate
	 * @throws Exception: throws Exception if the robot doesn't have enough energy to rotate
	 */
	public void rotate(Turn direction) throws OutOfEnergyException{
		robot.rotate(direction);
		if(direction.equals(Turn.LEFT)){
			maze.rotate(1);
			energyConsumed += robot.getEnergyForFullRotation()/4;
		}
		else if(direction.equals(Turn.RIGHT)){
			maze.rotate(-1);
			energyConsumed += robot.getEnergyForFullRotation()/4;
		}
		else{
			maze.rotate(1);
			maze.rotate(1);
			energyConsumed += robot.getEnergyForFullRotation()/2;
		}
	}

	/**
	 * Does distance sensing for the robot's right
	 * @return the distance of the obstacle on the robot's right
	 */
	private int senseRight() throws OutOfEnergyException{
		rotate(Turn.RIGHT);
		int ret = robot.distanceToObstacle(Direction.FORWARD);
		rotate(Turn.LEFT);
		return ret;
	}

	/**
	 * Encapsulates distance sensing and accounts for the lack for right distance sensor
	 * @param direction: direction of the distance sensing
	 * @return distnace to the obstacle of the given direction
	 */
	protected int distanceToObstacle(Direction direction) throws OutOfEnergyException{
		energyConsumed++;
		if(direction == Direction.RIGHT){
			return senseRight();
		}
		else if(direction == Direction.BACKWARD){
			return -1;
		}
		else{
			return robot.distanceToObstacle(direction);
		}
	}

	public void setMaze(Maze maze){
		this.maze = maze;
	}

	public Robot getRobot(){
		return robot;
	}

	public boolean drive2ExitOneStep() throws OutOfEnergyException, Exception{
		return true;
	}

	protected boolean insideMaze(int x, int y){
		return x >= 0 && y >= 0 && x < width && y < height;
	}
}
