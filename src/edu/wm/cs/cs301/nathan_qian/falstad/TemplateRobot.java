package edu.wm.cs.cs301.nathan_qian.falstad;

/**
 * Uses template design pattern. Defines common robot behavior.
 * @author Nathan Qian, Helen Li
 *
 */
public abstract class TemplateRobot implements Robot {

	protected float energy;
	public int xPosition;
	public int yPosition; 
	protected boolean hasStopped;
	int dir;
	Maze maze;

	@Override
	/**
	 * Rotates the robot with the given turn direction
	 * Throws Exception if the robot doesn't have enough energy to rotate
	 */
	public void rotate(Turn turn) throws OutOfEnergyException {
		// if robot has enough energy and has not stopped
		if(energy < getEnergyForFullRotation()/4){
			hasStopped = true;
			throw new OutOfEnergyException("Robot has run out of energy");
			//set hasstopped to true

		}
		if(turn == Robot.Turn.LEFT){

			dir = (dir + 1) % 4;
			energy = energy - (getEnergyForFullRotation()/4);
		}
		else if(turn == Robot.Turn.RIGHT){

			dir = (dir + 3) % 4;
			energy = energy - (getEnergyForFullRotation()/4);
		}
		else if(turn == Robot.Turn.AROUND){
			dir = (dir + 1) % 4;
			energy = energy - (getEnergyForFullRotation()/4); //rotate once 

			if(energy < getEnergyForFullRotation()/4){
				hasStopped = true;
				throw new OutOfEnergyException(); //check to see if it can keep going 
				//set has stopped to true 

			}

			dir = (dir + 1) % 4;
			energy = energy - (getEnergyForFullRotation()/4); //rotate again if it can keep going 

		}
		// decrement by 3
		// else
		// throw exception
	}

	@Override
	/**
	 * Moves the robot the given distance
	 * Throws Exception if the robot doesn't have enough energy to move
	 */
	public void move(int distance) throws Exception, OutOfEnergyException, CollisionException {
		// if robot has enough energy and has not stopped
		if(distance < 0){
			throw new Exception();
		}
		for(int i = 0; i < distance; i++){
			if(energy < getEnergyForStepForward()){
				hasStopped = true;
				throw new OutOfEnergyException("Robot has stopped");
			}
			else if(distanceToObstacle(Robot.Direction.FORWARD) == 0){
				throw new CollisionException("Obstacle blocking robot");
			}
			energy = energy - getEnergyForStepForward();
			if(dir == 0){
				xPosition++;
			}
			else if(dir == 1){
				yPosition++;
			}
			else if(dir == 2){
				xPosition--;
			}
			else{
				yPosition--;
			}
		}
		// decrement by 5 * distance
		// else if robot doesn't have enough energy and has stopped
		// throw exception
		// else (robot has enough energy and there's an obstacle in the way)
		// robot doesn't move
	}

	@Override
	/**
	 * Returns the robot's position
	 * Throws Exception if the robot is at an invalid coordinate
	 */
	public int[] getCurrentPosition() throws Exception {
		// robot stores position and simply returns this attribute
		if(xPosition < 0 || yPosition < 0 || xPosition >= maze.mazew || yPosition >= maze.mazeh){
			throw new ArrayIndexOutOfBoundsException();
		}
		int[] ret = {xPosition, yPosition};
		return ret;
	}

	@Override
	public void setMaze(Maze maze) {
		// set the maze attribute of the robot
		this.maze = maze;
	}

	@Override
	/**
	 * Checks if the robot is at the exit cell
	 */
	public boolean isAtGoal() {
		// uses getCurrentPosition()
		int[][] distance = maze.mazedists.getDists();
		if(distance[xPosition][yPosition] == 1){
			return true;
		}
		else{
			return false;
		}
	}

	/**
	 * Converts Robot.Direction into an integer for checks involving Directions
	 * @param direction
	 * @return an integer representation of direction
	 */
	protected int directionNumber(Direction direction){
		if(direction == Robot.Direction.LEFT){
			return 1;
		}
		else if(direction == Robot.Direction.BACKWARD){
			return 2;
		}
		else if(direction == Robot.Direction.RIGHT){
			return 3;
		}
		else{
			return 0;
		}
	}

	@Override
	/**
	 * Checks if the robot can see the goal with the given direction
	 * Throws UnsupporedOperationException if the robot doesn't have the appropriate distance sensor
	 */
	public boolean canSeeGoal(Direction direction)
			throws UnsupportedOperationException {
		// check with the maze to find the exit

		// Figure out battery level
		// If the the exit is at the same direction of the robot, return true

		// tempdir = (dir + direction)%4
		// if tempdir == 0
		// check hasWallOnLeft
		// else if tempdir == 1
		// check hasWallOnBottom
		// else if tempdir == 2
		// check hasWallOnRight
		// else tempdir == 3
		// check hasWallOnTop

		if(hasDistanceSensor(direction) == false){
			throw new UnsupportedOperationException();
		}
		Cells mazeCells = maze.mazecells;
		int tempx = xPosition;
		int tempy = yPosition;
		int tempdir = (dir + directionNumber(direction))%4;
		if(tempdir == 0){
			int deltax = 1;
			// The exit out of the maze returns null on hasNoWallOnLeft
			while(mazeCells.hasNoWallOnBottom(tempx, tempy)){
				if(mazeCells.isExitPosition(tempx, tempy) && maze.isOutsideMaze(tempx, tempy + deltax)){
					return true;
				}
				else{
					// implementing
					tempy = tempy + deltax; 
				}
			}
		}
		else if(tempdir == 1){
			int deltay = -1;
			while(mazeCells.hasNoWallOnLeft(tempx, tempy)){
				if(mazeCells.isExitPosition(tempx, tempy) && maze.isOutsideMaze(tempx + deltay, tempy)){
					return true;
				}
				else{
					tempx = tempx + deltay; 
				}
			}
		}
		else if(tempdir == 2){
			int deltax = -1;
			while(mazeCells.hasNoWallOnTop(tempx, tempy)){
				if(mazeCells.isExitPosition(tempx, tempy) && maze.isOutsideMaze(tempx, tempy + deltax)){
					return true;
				}
				else{
					// figure out how to increment tempx and tempy correctly
					tempy = tempy + deltax; 
				}	
			}
		}
		else{
			// dy = -1
			// (5, 0) --> (6, 0)
			// deltax = 0, deltay = 1
			int deltay = 1;
			while(mazeCells.hasNoWallOnRight(tempx, tempy)){
				if(mazeCells.isExitPosition(tempx, tempy) && maze.isOutsideMaze(tempx + deltay, tempy)){
					return true;
				}
				else{
					tempx = tempx + deltay; 
					// figure out how to increment tempx and tempy correctly
				}
			}
		}
		// update energy if sensing distance
		energy--;
		return false;
	}

	/**
	 * Counts the number of walls surrounding the cell that the robot is in.
	 * @return the number of walls
	 */
	protected int countWalls(){
		Cells mazeCells = maze.mazecells;
		int count = 0;
		if(mazeCells.hasWallOnLeft(xPosition, yPosition)){
			count++;
		}
		if(mazeCells.hasWallOnTop(xPosition, yPosition)){
			count++;
		}
		if(mazeCells.hasWallOnRight(xPosition, yPosition)){
			count++;
		}
		if(mazeCells.hasWallOnBottom(xPosition, yPosition)){
			count++;
		}
		return count;
	}

	@Override
	public boolean isAtJunction() throws UnsupportedOperationException {
		// check with the maze to retrieve cells
		if(hasJunctionSensor() == false){
			throw new UnsupportedOperationException();
		}
		else{
			if(countWalls() <= 1){
				return true;
			}
			else{
				return false;
			}
		}
		// check if the cell has any neighbor on either sides 
	}

	@Override
	public boolean hasJunctionSensor() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isInsideRoom() throws UnsupportedOperationException {
		// access Maze.mazecells 
		if(hasRoomSensor()){
			Cells mazeCells = maze.mazecells;
			return mazeCells.isInRoom(xPosition, yPosition);
		}
		else{
			throw new UnsupportedOperationException();
		}
	}

	@Override
	public boolean hasRoomSensor() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int[] getCurrentDirection() {
		// return direction
		int[] ret = {Constants.DIRS_X[dir], Constants.DIRS_Y[dir]};
		return ret;
	}

	@Override
	public float getBatteryLevel() {
		return energy;
	}

	@Override
	public void setBatteryLevel(float level) {
		energy = level;
	}

	@Override
	public float getEnergyForFullRotation() {
		// TODO Auto-generated method stub
		return 3;
	}

	@Override
	public float getEnergyForStepForward() {
		// TODO Auto-generated method stub
		return 5;
	}

	@Override
	public boolean hasStopped() {
		return hasStopped;
	}

	@Override
	public int distanceToObstacle(Direction direction)
			throws UnsupportedOperationException {
		if(hasDistanceSensor(direction) == false){ //throws the exception 
			throw new UnsupportedOperationException();
		}
		Cells mazeCells = maze.mazecells;
		int tempx = xPosition;
		int tempy = yPosition;
		int counter = 0; //counter to be returned later
		int tempdir = (dir + directionNumber(direction))%4;
		if(tempdir == 0){
			int deltax = 1;
			while(!maze.isOutsideMaze(tempx, tempy) && mazeCells.hasNoWallOnRight(tempx, tempy)){
				tempx += deltax;  
				counter++; 
			}
		}
		else if(tempdir == 1){
			int deltay = 1;
			while(!maze.isOutsideMaze(tempx, tempy) && mazeCells.hasNoWallOnBottom(tempx, tempy)){
				tempy += deltay;  
				counter++; 
			}
		}
		else if(tempdir == 2){
			int deltax = -1;
			while(!maze.isOutsideMaze(tempx, tempy) && mazeCells.hasNoWallOnLeft(tempx, tempy)){
				tempx += deltax; 
				counter++; 
			}
		}
		else{
			int deltay = -1;
			while(!maze.isOutsideMaze(tempx, tempy) && mazeCells.hasNoWallOnTop(tempx, tempy)){
				tempy += deltay; 
				counter++; 
			}
		}
		// update energy if sensing distance
		energy--;
		return counter;
	}

	@Override
	public boolean hasDistanceSensor(Direction direction) {
		// TODO Auto-generated method stub
		return false;
	}

}
