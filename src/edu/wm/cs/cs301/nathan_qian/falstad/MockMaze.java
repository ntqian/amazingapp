package edu.wm.cs.cs301.nathan_qian.falstad;

public class MockMaze extends Maze{

	public MockMaze(){

	}
	
	public MockMaze(int i, int skill) {
		// TODO Auto-generated constructor stub
		method = i;
		this.skill = skill;
	}

	public void newMaze(BSPNode root, Cells c, Distance dists, int startx, int starty){
		//showMaze = showSolution = solving = false;
		mazecells = c ;
		mazedists = dists;
		seencells = new Cells(mazew+1,mazeh+1) ;
		rootnode = root ;
		walkStep = 0;
		viewdx = dx<<16; 
		viewdy = dy<<16;
		angle = 0;
		//mapMode = false;
		// set the current state for the state-dependent behavior
		//state = Constants.STATE_PLAY;
	}
	
	public void init(){
		
	}
}
