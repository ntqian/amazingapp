package edu.wm.cs.cs301.nathan_qian.falstad;

import android.util.Log;
import edu.wm.cs.cs301.nathan_qian.falstad.Robot.Turn;

public class Wizard extends TemplateDriver {

	/**
	 * Constructor
	 */
	public Wizard(){
		energyConsumed = 0;
		distanceTraveled = 0;
		width = 0;
		height = 0;
		distance = null;
		maze = null;
		robot = null;
	}

	@Override
	/**
	 * Doesn't need sensors to operate
	 */
	public void setRobot(Robot r) throws UnsuitableRobotException {
		Log.v("Wizard", "Wizard's setRobot()");
		// TODO Auto-generated method stub
		if(!r.equals(null)){
			robot = (TemplateRobot) r;
		}
		else{
			throw new UnsuitableRobotException();
		}
	}

	/**
	 * Finds the direction of the neighboring cell that has a distance of one less than the robot's current cell
	 * @param xPosition
	 * @param yPosition
	 * @param distances
	 * @return neighbor's cell direction relative to robot's current cell
	 */
	private int getOneLessDistance(int xPosition, int yPosition, int[][] distances) {
		if(insideMaze(xPosition+1, yPosition) && distances[xPosition][yPosition] - distances[xPosition+1][yPosition] == 1){
			return 0;
		}
		else if(insideMaze(xPosition, yPosition+1) && distances[xPosition][yPosition] - distances[xPosition][yPosition+1] == 1){
			return 1;
		}
		else if(insideMaze(xPosition-1, yPosition) && distances[xPosition][yPosition] - distances[xPosition-1][yPosition] == 1){
			return 2;
		}
		else if(insideMaze(xPosition, yPosition-1) && distances[xPosition][yPosition] - distances[xPosition][yPosition-1] == 1){
			return 3;
		}
		else{
			return -1;
		}
	}

	@Override
	/**
	 * Implementation of Wizard algorithm
	 */
	public boolean drive2Exit() throws Exception {
		int[][] distances = distance.getDists();
		int[] position = robot.getCurrentPosition();
		int xPosition = position[0];
		int yPosition = position[1];
		// while celldistance != 1 
		while(distances[xPosition][yPosition] != 1){
			// check where the borders are - if no border, add them to an array
			// for cells in array, check distances
			// go for the first one that is current distance-1
			int neighborDirection = getOneLessDistance(xPosition, yPosition, distances);
			if(robot.getBatteryLevel() < 3){
				throw new Exception();
			}
			else if(neighborDirection == ((robot.dir)+1) % 4){
				rotate(Turn.LEFT);
			}
			else if(neighborDirection == ((robot.dir)+3) % 4){
				rotate(Turn.RIGHT);
			}
			if(robot.getBatteryLevel() < 6){
				throw new Exception();
			}
			else if(neighborDirection == ((robot.dir)+2) % 4){
				// TODO: fix rotate for all the drivers
				rotate(Turn.AROUND);
				energyConsumed += robot.getEnergyForFullRotation()/2;
				//				maze.keyDown(null, LEFT);
				//				maze.keyDown(null, LEFT);
			}
			if(robot.getBatteryLevel() < 5){
				throw new Exception();
			}
			move(1);
			position = robot.getCurrentPosition();
			xPosition = position[0];
			yPosition = position[1];
		}
		return true;
	}

	public boolean drive2ExitOneStep() throws Exception, OutOfEnergyException{
		int[][] distances = distance.getDists();
		int[] position = robot.getCurrentPosition();
		int xPosition = position[0];
		int yPosition = position[1];
		int neighborDirection = getOneLessDistance(xPosition, yPosition, distances);
		if(neighborDirection == ((robot.dir)+1) % 4){
			rotate(Turn.LEFT);
		}
		else if(neighborDirection == ((robot.dir)+3) % 4){
			rotate(Turn.RIGHT);
		}
		else if(neighborDirection == ((robot.dir)+2) % 4){
			rotate(Turn.AROUND);
			energyConsumed += robot.getEnergyForFullRotation()/2;
		}
		move(1);
		return true;
	}
}
