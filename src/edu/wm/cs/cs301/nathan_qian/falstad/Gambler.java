package edu.wm.cs.cs301.nathan_qian.falstad;

import java.util.ArrayList;

import android.util.Log;
import edu.wm.cs.cs301.nathan_qian.falstad.Robot.Direction;
import edu.wm.cs.cs301.nathan_qian.falstad.Robot.Turn;

public class Gambler extends TemplateDriver{

	protected SingleRandom random;

	/**
	 * Constructor
	 * Instantiates SingleRandom to add the random attribute of the Gambler
	 */
	public Gambler(){
		energyConsumed = 0;
		distanceTraveled = 0;
		width = 0;
		height = 0;
		distance = null;
		maze = null;
		SingleRandom.setSeed(6942);
		random = SingleRandom.getRandom();
		robot = null;
	}

	@Override
	/**
	 * Checks if the robot has at least a junction sensor
	 */
	public void setRobot(Robot r) throws UnsuitableRobotException {
		Log.v("Gambler", "Gambler's setRobot()");
		if(!r.equals(null) && r.hasJunctionSensor()){
			robot = (TemplateRobot) r;
		}
		else{
			throw new UnsuitableRobotException();
		}
	}

	/**
	 * Generates possible paths the robot can travel from a junction
	 * @return returns an ArrayList with possible directions the robot can turn to travel
	 */
	private ArrayList<Turn> getDirections() throws OutOfEnergyException{
		ArrayList<Turn> ret = new ArrayList<Turn>();
		if(distanceToObstacle(Robot.Direction.LEFT) > 0){
			ret.add(Robot.Turn.LEFT);
		}
		else if(distanceToObstacle(Robot.Direction.FORWARD) > 0){
			ret.add(null);
		}
		else if(distanceToObstacle(Robot.Direction.RIGHT) > 0){
			ret.add(Robot.Turn.RIGHT);
		}
		return ret;
	}

	@Override
	/**
	 * Implementation of Gambler algorithm
	 */
	public boolean drive2Exit() throws Exception {
		// TODO Auto-generated method stub
		while(!robot.isAtGoal()){
			System.out.println(robot.getBatteryLevel());
			ArrayList<Turn> paths;
			// If the robot is at a junction
			if(robot.isAtJunction()){
				// Choose a random direction available to turn (at most, left, right or no turn)
				paths = getDirections();
				int index = random.nextIntWithinInterval(0, paths.size()-1);
				Turn turn = paths.get(index);
				if(turn != null){
					rotate(turn);
				}
				// Move forward one
				move(1);
			}
			// Else if the robot is at a dead end
			else if(distanceToObstacle(Direction.FORWARD) == 0){
				if(distanceToObstacle(Direction.LEFT) > 0){
					rotate(Turn.LEFT);
				}
				else if(distanceToObstacle(Direction.RIGHT) > 0){
					rotate(Turn.RIGHT);
				}
				else{
					rotate(Turn.AROUND);
				}
			}
			// Else
			else{
				// Move forward one
				move(1);
			}
		}
		return true;
	}

	@Override
	public boolean drive2ExitOneStep() throws OutOfEnergyException, Exception {
		ArrayList<Turn> paths;
		// If the robot is at a junction
		if(robot.isAtJunction()){
			// Choose a random direction available to turn (at most, left, right or no turn)
			paths = getDirections();
			int index = random.nextIntWithinInterval(0, paths.size()-1);
			Turn turn = paths.get(index);
			if(turn != null){
				rotate(turn);
			}
			// Move forward one
			move(1);
		}
		// Else if the robot is at a dead end
		else if(distanceToObstacle(Direction.FORWARD) == 0){
			if(distanceToObstacle(Direction.LEFT) > 0){
				rotate(Turn.LEFT);
			}
			else if(distanceToObstacle(Direction.RIGHT) > 0){
				rotate(Turn.RIGHT);
			}
			else{
				rotate(Turn.AROUND);
			}
		}
		// Else
		else{
			// Move forward one
			move(1);
		}
		return true;
	}
}
