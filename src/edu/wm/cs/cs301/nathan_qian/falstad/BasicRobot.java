package edu.wm.cs.cs301.nathan_qian.falstad;

public class BasicRobot extends TemplateRobot {

	/**
	 * Constructors for basic robot. BasicRobot will have all sensors on them.
	 */
	public BasicRobot(){
		energy = 2500;
		maze = null;
		xPosition = 0;
		yPosition = 0;
		dir = 0;
		hasStopped = false;
	}

	public BasicRobot(int x, int y){
		energy = 2500;
		maze = null;
		xPosition = x;
		yPosition = y;
		dir = 0;
		hasStopped = false;
	}

	public BasicRobot(int x, int y, int direction){
		energy = 2500;
		maze = null;
		xPosition = x;
		yPosition = y;
		dir = direction;
		hasStopped = false;
	}

	@Override
	/**
	 * Returns true if the robot has a junction sensor, false otherwise
	 */
	public boolean hasJunctionSensor() {
		// check if robot has junction sensor
		return true;
	}

	@Override
	/**
	 * Returns true if the robot has a room sensor, false otherwise
	 */
	public boolean hasRoomSensor() {
		// check if robot has room sensor
		return true;
	}

	@Override
	/**
	 * Returns energy needed for a full rotation
	 */
	public float getEnergyForFullRotation() {
		// return energy needed for full rotation
		return 12;
	}

	@Override
	/**
	 * Returns energy needed for a single step forward
	 */
	public float getEnergyForStepForward() {
		// return energy needed for a single step forward
		return 5;
	}

	@Override
	/**
	 * Returns true if the robot has a distance sensor in the given direction, false otherwise
	 */
	public boolean hasDistanceSensor(Direction direction) {
		// check if robot has a direction sensor for the given direction
		return true;
	}
}
