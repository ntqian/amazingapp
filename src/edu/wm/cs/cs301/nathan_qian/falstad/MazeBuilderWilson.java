package edu.wm.cs.cs301.nathan_qian.falstad;

import java.util.ArrayList;
import java.util.HashMap;

public class MazeBuilderWilson extends MazeBuilder{
	protected int rooms;

	public MazeBuilderWilson(){
		SingleRandom.setSeed(89273);
		random = SingleRandom.getRandom();
	}

	public MazeBuilderWilson(boolean deterministic){
		super(deterministic);
	}

	public void build(Maze mz, int w, int h, int roomct, int pc){
		init(mz, w, h, roomct, pc);
		buildThread = new Thread(this);
		buildThread.start();
	}

	public void init(Maze mz, int w, int h, int roomct, int pc){
		maze = mz;
		width = w;
		height = h;
		rooms = roomct;
		expectedPartiters = pc;
		cells = new Cells(w,h) ;
		dists = new Distance(w,h) ;
	}

	protected void generate(){
		ArrayList<String> notVisited = new ArrayList<String>();
		ArrayList<String> visited = new ArrayList<String>();
		HashMap<String, String> wilson = new HashMap<String, String>();

		for(int i =0; i < width; i++){
			for(int j = 0; j < height; j++){
				notVisited.add("" + i + "," + j);
			}
		}

		int x = random.nextIntWithinInterval(0, width-1);
		int y = random.nextIntWithinInterval(0, height-1);
		cells.setCellAsVisited(x, y);
		visited.add("" + x + "," + y);
		notVisited.remove("" + x + "," + y);

		int rando[] = randoXY(notVisited);
		int randox = rando[0];
		int randoy = rando[1];
		int previousX = -1;		// indicates that the cell if it maps to (-1, -1) is the start of the walk
		int previousY = -1;
		wilson.put("" + randox + "," + randoy, "" + previousX + "," + previousY);
		while(notVisited.size() > 0){
			//System.out.println("Coordinates: " + randox + "," + randoy);
			//System.out.println("Checking in wilson: " + wilson.containsKey("" + randox + "," + randoy));
			//System.out.println("Checking in visited: " + visited.contains("" + randox + "," + randoy));

			for(int i = 0; i < visited.size(); i++){
				System.out.println("Visited: " + visited.get(i) + " ");
			}
			if(wilson.containsKey("" + randox + "," + randoy) == true){
				// randox and randoy become previousX and previousY to remap this cell
				previousX = randox;
				previousY = randoy;
			}
			else if(visited.contains("" + randox + "," + randoy) == true ){
				// delete cell walls
				while(wilson.get("" + randox + "," + randoy) != "-1,-1"){
					System.out.println("notVisited size: " + notVisited.size());
					System.out.println("" + randox + "," + randoy);
					System.out.println(wilson.get("" + randox + "," + randoy));
					int[] previousInt = coordinateToInteger(wilson.get("" + randox + "," + randoy));
					previousX = previousInt[0];
					previousY = previousInt[1];
					deleteWalls(randox, randoy, previousX, previousY);
					// move everything in wilson to visited
					visited.add("" + randox + "," + randoy);
					// remove everything in wilson from notVisited
					notVisited.remove("" + randox + "," + randoy);
					randox = previousX;
					randoy = previousY;
					System.out.println("Working on inner loop");
				}
				// clear wilson
				wilson.clear();
				// reset previousX and previousY
				previousX = -1;
				previousY = -1;
				// reset randox and randoy
				rando = randoXY(notVisited);
				randox = rando[0];
				randoy = rando[1];
			}
			wilson.put("" + randox + "," + randoy, "" + previousX + "," + previousY);
			cells.setCellAsVisited(randox, randoy);
			previousX = randox;
			previousY = randoy;
			rando = generateValidCoordinate(randox, randoy);
			randox += rando[0];
			randoy += rando[1];
			System.out.println("Working on outer loop");
		}
		final int[] remote = dists.computeDistances(cells);
		cells.setExitPosition(remote[0], remote[1]);
	}

	protected void deleteWalls(int randox, int randoy, int previousX, int previousY){
		int dx = randox - previousX;
		int dy = randoy - previousY;
		cells.deleteWall(randox, randoy, dx, dy);
	}

	protected int[] randoXY(ArrayList<String> notVisited){
		int index = random.nextIntWithinInterval(0, notVisited.size()-1);
		String coordinate = notVisited.get(index);
		return coordinateToInteger(coordinate);
	}

	protected int[] coordinateToInteger(String coordinate){
		String[] out = coordinate.split(",");
		int[] ret = new int[2];
		ret[0] = Integer.parseInt(out[0]);
		ret[1] = Integer.parseInt(out[1]);
		return ret;
	}

	/**
	 * Method generates a random value for dx and dy
	 * @param upperBound Describes the height or width
	 * @param variable X or Y
	 * @return new value of X or Y
	 **/
	protected int[] generateValidCoordinate(int x, int y){
		ArrayList<Integer> list = new ArrayList<Integer>();
		if(x < width-1){
			list.add(0);
		}
		if(y < height-1){
			list.add(1);
		}
		if(x > 0){
			list.add(2);
		}
		if(y > 0){
			list.add(3);
		}
		int index = random.nextIntWithinInterval(0, list.size()-1);
		int[] array = new int[2];
		int dx = Constants.DIRS_X[list.get(index)];
		int dy = Constants.DIRS_Y[list.get(index)];
		array[0] = dx;
		array[1] = dy;
		return array;
	}

	public void run(){
		try{
			cells.initialize();
			//generateRooms();
			System.out.println("Generated rooms");

			Thread.sleep(SLEEP_INTERVAL);

			generate();
			System.out.println("Generated pathways");

			Thread.sleep(SLEEP_INTERVAL);

			final int colchange = random.nextIntWithinInterval(0, 255); // used in the constructor for Segments  class Seg
			final BSPBuilder b = new BSPBuilder(maze, dists, cells, width, height, colchange, expectedPartiters) ;
			BSPNode root = b.generateBSPNodes();

			Thread.sleep(SLEEP_INTERVAL);

			maze.newMaze(root, cells, dists, startx, starty);
			System.out.println("Generated maze");
		}
		catch(InterruptedException ex){
			dbg("Catching signal to stop");
		}
	}

	public static void main(String[] args){
		int randox = 1;
		int randoy = 2;
		int previousX = 3;
		int previousY = 9;
		ArrayList<String> visited = new ArrayList<String>();
		visited.add("" + previousX + "," + previousY);
		HashMap<String, String> wilson = new HashMap<String, String>();
		wilson.put("1,2", "-1,-1");
		System.out.println(wilson.containsKey("" + previousX + "," + previousY));
		System.out.println(wilson.containsKey("" + randox + "," + randoy));
		System.out.println(wilson.get("" + randox + "," + randoy) == "-1,-1");
		System.out.println(visited.contains("" + previousX + "," + previousY));
	}
}
