package edu.wm.cs.cs301.nathan_qian.falstad;

import java.util.HashMap;

public class CustomRobot extends TemplateRobot {
	
	protected boolean roomSensor;
	protected boolean junctionSensor;
	protected HashMap<Direction, Boolean> distanceSensor;
	
	/**
	 * Default constructor
	 */
	public CustomRobot(){
		this(0, 0, 0, false, false, false, false, false, false);
	}
	
	/**
	 * Constructor for specific location and direction
	 * @param x: x-coordinate
	 * @param y: y-coordiante
	 * @param direction: direction of the robot
	 */
	public CustomRobot(int x, int y, int direction){
		this(x, y, direction, false, false, false, false, false, false);
	}
	
	/**
	 * Constructor for specific sensors
	 * @param r: room sensor
	 * @param j: junciton sensor
	 * @param forward: forward distance sensor
	 * @param left: left distance sensor
	 * @param backward: backward distance sensor
	 * @param right: right distance sensor
	 */
	public CustomRobot(boolean r, boolean j, boolean forward, boolean left, boolean backward, boolean right){
		this(0, 0, 0, r, j, forward, left, backward, right);
	}
	
	/**
	 * Constructor for specific location, direction, and sensors
	 * @param x: x-coordinate
	 * @param y: y-coordiante
	 * @param direction: direction of the robot
	 * @param r: room sensor
	 * @param j: junction sensor
	 * @param forward: forward distance sensor
	 * @param left: left distance sensor
	 * @param backward: backward distance sensor
	 * @param right: right distance sensor:
	 */
	public CustomRobot(int x, int y, int direction, boolean r, boolean j, boolean forward, boolean left, boolean backward, boolean right){
		energy = 2500;
		maze = null;
		xPosition = x;
		yPosition = y;
		dir = direction;
		hasStopped  = false;
		roomSensor = r;
		junctionSensor = j;
		distanceSensor = new HashMap<Direction, Boolean>();
		distanceSensor.put(Direction.FORWARD, forward);
		distanceSensor.put(Direction.LEFT, left);
		distanceSensor.put(Direction.BACKWARD, backward);
		distanceSensor.put(Direction.RIGHT, right);
	}

	public boolean hasJunctionSensor(){
		return junctionSensor;
	}
	
	public boolean hasRoomSensor(){
		return roomSensor;
	}
	
	public float getEnergyForFullRotation(){
		return 12;
	}
	
	public float getEnergyForStepForward(){
		return 5;
	}
	
	public boolean hasDistanceSensor(Direction direction){
		return distanceSensor.get(direction);
	}
}
