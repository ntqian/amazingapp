package edu.wm.cs.cs301.nathan_qian.falstad;

import android.graphics.Paint;

public class Tuple {

	public Object shape;
	public Paint paint;
	
	public Tuple(Object o, Paint paint){
		shape = o;
		this.paint = paint;
	}
	
	
}
