package edu.wm.cs.cs301.nathan_qian.falstad;

import android.util.Log;
import edu.wm.cs.cs301.nathan_qian.falstad.Robot.Direction;
import edu.wm.cs.cs301.nathan_qian.falstad.Robot.Turn;

public class WallFollower extends TemplateDriver {

	/**
	 * Constructor
	 */
	public WallFollower(){
		energyConsumed = 0;
		distanceTraveled = 0;
		width = 0;
		height = 0;
		distance = null;
		robot = null;
	}

	@Override
	/**
	 * Checks if the robot has at least a distance sensor on the left and front and a room sensor
	 */
	public void setRobot(Robot r) throws UnsuitableRobotException {
		Log.v("WallFollower", "WallFollower's setRobot()");
		if(!r.equals(null) && r.hasDistanceSensor(Robot.Direction.LEFT) 
				&& r.hasDistanceSensor(Robot.Direction.FORWARD) && r.hasRoomSensor()){
			robot = (TemplateRobot) r;
		}
		else{
			throw new UnsuitableRobotException();
		}
	}

	@Override
	/**
	 * Implementation of WallFollower algorithm
	 */
	public boolean drive2Exit() throws Exception {
		// TODO Auto-generated method stub
		// While robot is not at goal
		while(!robot.isAtGoal()){
			if(distanceToObstacle(Direction.LEFT) == 0 && distanceToObstacle(Direction.FORWARD) > 0){
				move(1);
			}
			else if(distanceToObstacle(Direction.LEFT) > 0){
				rotate(Turn.LEFT);
				move(1);
			}
			else if(distanceToObstacle(Direction.LEFT) == 0 && distanceToObstacle(Direction.FORWARD) == 0){
				rotate(Turn.RIGHT);
			}
			else if(distanceToObstacle(Direction.FORWARD) > 0 && robot.isInsideRoom()){
				move(1);
			}
			else if(distanceToObstacle(Direction.LEFT) > 0 && robot.isInsideRoom()){
				rotate(Turn.LEFT);
				move(1);
			}
		}
		// If the distance to obstacle of the robot's left is zero
		// Move forward one
		// Else if distance to obstacle on the robot's front is zero
		// rotate robot right
		// Else if the distance to obstacle on the robot's left is greater than zero and the robot is in a room
		// Move forward one
		// Else
		// Turn the robot left
		// Move forward one
		return true;
	}

	@Override
	public boolean drive2ExitOneStep() throws OutOfEnergyException, Exception {
		if(distanceToObstacle(Direction.LEFT) == 0 && distanceToObstacle(Direction.FORWARD) > 0){
			move(1);
		}
		else if(distanceToObstacle(Direction.LEFT) > 0){
			rotate(Turn.LEFT);
			move(1);
		}
		else if(distanceToObstacle(Direction.LEFT) == 0 && distanceToObstacle(Direction.FORWARD) == 0){
			rotate(Turn.RIGHT);
		}
		else if(distanceToObstacle(Direction.FORWARD) > 0 && robot.isInsideRoom()){
			move(1);
		}
		else if(distanceToObstacle(Direction.LEFT) > 0 && robot.isInsideRoom()){
			rotate(Turn.LEFT);
			move(1);
		}
		// If the distance to obstacle of the robot's left is zero
		// Move forward one
		// Else if distance to obstacle on the robot's front is zero
		// rotate robot right
		// Else if the distance to obstacle on the robot's left is greater than zero and the robot is in a room
		// Move forward one
		// Else
		// Turn the robot left
		// Move forward one
		return true;
	}
}
