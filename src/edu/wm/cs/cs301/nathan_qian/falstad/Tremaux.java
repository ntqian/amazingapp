package edu.wm.cs.cs301.nathan_qian.falstad;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

import android.util.Log;
import edu.wm.cs.cs301.nathan_qian.falstad.Robot.Direction;
import edu.wm.cs.cs301.nathan_qian.falstad.Robot.Turn;

public class Tremaux extends TemplateDriver {

	protected SingleRandom random;
	HashMap<String, String> junctions = new HashMap<String, String>();
	HashMap<String, ArrayList<Integer>> paths = new HashMap<String, ArrayList<Integer>>();
	Stack<Direction> moves = new Stack<Direction>();

	final private static String STARTINGROOM = "-1,-1";
	final private static String STARTINGJUNCTION = "-2,-2";

	/**
	 * Constructor
	 */
	public Tremaux(){
		energyConsumed = 0;
		distanceTraveled = 0;
		width = 0;
		height = 0;
		distance = null;
		maze = null;
		robot = null;
		SingleRandom.setSeed(6942);
		random = SingleRandom.getRandom();
	}

	@Override
	/**
	 * Checks if the provided robot meets the minimum requirements of Tremaux driver
	 * @param r: provided robot
	 */
	public void setRobot(Robot r) throws UnsuitableRobotException {
		Log.v("Tremaux", "Tremaux's setRobot()");
		if(!r.equals(null) && r.hasJunctionSensor() && r.hasDistanceSensor(Direction.FORWARD) 
				&& r.hasDistanceSensor(Direction.LEFT) && !r.hasDistanceSensor(Direction.RIGHT) 
				&& !r.hasDistanceSensor(Direction.BACKWARD) && r.hasRoomSensor()){
			robot = (TemplateRobot) r;
		}
		else{
			throw new UnsuitableRobotException();
		}
	}

	/**
	 * Directs the robot back to the previous junction when called
	 * @throws Exception: throws Exception if robot donesn't have enough energy to rotate or move
	 */
	private void backtrack() throws OutOfEnergyException, Exception{
		Direction direction;
		do{
			direction = moves.pop();
			if(direction == Direction.FORWARD){
				move(1);
			}
			else if(direction == Direction.LEFT){
				rotate(Turn.RIGHT);
			}
			else if(direction == Direction.RIGHT){
				rotate(Turn.LEFT);
			}
		}while(!robot.isAtJunction());
		direction = moves.peek();
		if(direction == Direction.LEFT){
			moves.pop();
			moves.add(Direction.RIGHT);
		}
		else if(direction == Direction.RIGHT){
			moves.pop();
			moves.add(Direction.LEFT);
		}
	}

	/**
	 * Converts an integer array representation of coordinate to a string representation of a coordinate
	 * @param position: integer array representation of a coordinate
	 * @return string representation of the coordiante
	 */
	private String positionToString(int[] position){
		return "" + position[0] + "," + position[1] + "";
	}

	/**
	 * Checks if the robot is at a dead end
	 * @return true if there's a wall on the left and right and on the front, false otherwise
	 */
	private boolean isAtDeadEnd() throws OutOfEnergyException{
		if(distanceToObstacle(Direction.FORWARD) == 0 && distanceToObstacle(Direction.LEFT) == 0 && distanceToObstacle(Direction.RIGHT) == 0){
			return true;
		}
		else{
			return false;
		}
	}

	/**
	 * Directs the robot to a new junction or the exit if encountered. If the robot arrives at a new
	 * junction, it creates the the possible paths the robot can travel at the new junction
	 * @return if the robot starts at the exit, return true, else false otherwise
	 * @throws Exception: throws Exception if the robot doesn't have enough energy to rotate or move
	 */
	private boolean drive() throws Exception{
		if(robot.isAtGoal()){
			return true;
		}
		do{
			if(isAtDeadEnd()){
				return false;
			}
			if(distanceToObstacle(Direction.FORWARD) > 0){
				moves.add(Direction.FORWARD);
				move(1);
			}
			else if(distanceToObstacle(Direction.LEFT) > 0){
				moves.add(Direction.LEFT);
				rotate(Turn.LEFT);
			}
			else if(distanceToObstacle(Direction.RIGHT) > 0){
				moves.add(Direction.RIGHT);
				rotate(Turn.RIGHT);
			}
		}while(!robot.isAtJunction());
		if(!junctions.containsKey(robot.getCurrentPosition())){
			ArrayList<Integer> newPathList = createPathList();
			String position = positionToString(robot.getCurrentPosition());
			paths.put(position, newPathList);
		}
		return false;
	}

	/**
	 * Chooses a random path for the robot to explore at a junction
	 * @param pathList: an ArrayList of unexplored paths associated with the junction
	 * @throws Exception: throws Exception if robot doesn't have enough energy to move or rotate
	 */
	private void choosePath(ArrayList<Integer> pathList) throws Exception{
		int index = random.nextIntWithinInterval(0, pathList.size()-1);
		int direction = pathList.get(index);
		pathList.remove(index);
		int robotDirection = convertDirectionToInt(robot.getCurrentDirection());
		if((robotDirection + 1)%4 == direction){
			rotate(Turn.LEFT);
			moves.add(Direction.LEFT);
		}
		else if((robotDirection - 1)%4 == direction){
			rotate(Turn.RIGHT);
			moves.add(Direction.RIGHT);
		}
		move(1);
		moves.add(Direction.FORWARD);
	}

	/**
	 * Converts an integer array of direction to a integer representation of the direction
	 * @param dxdy: integer array of the direction
	 * @return integer representation of direction
	 */
	private int convertDirectionToInt(int[] dxdy){
		if(dxdy[0] == 0 && dxdy[1] == 1){
			return 0;
		}
		else if(dxdy[0] == -1 && dxdy[1] == 0){
			return 1;
		}
		else if(dxdy[0] == 0 && dxdy[1] == -1){
			return 2;
		}
		else if(dxdy[0] == 1 && dxdy[1] == 0){
			return 3;
		}
		else{
			return -1;
		}
	}

	/**
	 * Generates the possible paths from a junction
	 * @return ArrayList of paths with the directions that the robot can travel from the junction
	 */
	private ArrayList<Integer> createPathList() throws OutOfEnergyException{
		ArrayList<Integer> ret = new ArrayList<Integer>();
		int direction = convertDirectionToInt(robot.getCurrentDirection());
		if(distanceToObstacle(Direction.LEFT) > 0){
			ret.add((direction+1)%4);
		}
		if(distanceToObstacle(Direction.FORWARD) > 0){
			ret.add(direction);
		}
		if(distanceToObstacle(Direction.RIGHT) > 0){
			ret.add((direction+3)%4);
		}
		return ret;
	}

	/**
	 * Setups the Tremaux algorithm by moving the robot to a junction
	 * @return returns the position of the junction that the robot starts in as an integer array
	 * @throws Exception: throws Exception if the robot doesn't have enough energy to rotate or move
	 */
	private int[] startsInHallway() throws Exception{
		while(!robot.isAtJunction()){
			if(distanceToObstacle(Direction.FORWARD) > 0){
				move(1);
			}
			else if(robot.distanceToObstacle(Direction.LEFT) > 0){
				rotate(Turn.LEFT);
				move(1);
			}
			else if(distanceToObstacle(Direction.RIGHT) > 0){
				rotate(Turn.RIGHT);
				move(1);
			}
		}
		ArrayList<Integer> pathList = createPathList();
		String position = positionToString(robot.getCurrentPosition());
		paths.put(position, pathList);
		junctions.put(position, STARTINGJUNCTION);
		return robot.getCurrentPosition();
	}

	/**
	 * Setups the Tremaux algorithm by moving the robot to a junction in a room
	 * @return return the position of the junction that the robot starts in as an integer array
	 * @throws Exception: throws Exception if the robot doesn't have enough energy to rotate or move
	 */
	private int[] startsInRoom() throws Exception{
		int[] previousCell = robot.getCurrentPosition();
		while(true){
			if(distanceToObstacle(Direction.FORWARD) > 0 && robot.isInsideRoom()){
				previousCell = robot.getCurrentPosition();
				move(1);
			}
			else if(distanceToObstacle(Direction.LEFT) > 0 && robot.isInsideRoom()){
				previousCell = robot.getCurrentPosition();
				rotate(Turn.LEFT);
				move(1);
			}
			else if(distanceToObstacle(Direction.RIGHT) > 0 && robot.isInsideRoom()){
				previousCell = robot.getCurrentPosition();
				rotate(Turn.RIGHT);
				move(1);
			}
			else if(distanceToObstacle(Direction.FORWARD) > 0 && !robot.isInsideRoom()){
				String position = positionToString(previousCell);
				junctions.put(position, STARTINGROOM);
				return previousCell;
			}
		}
	}

	/**
	 * Determines with starting method to use if the robot starts in a hallway or in a room
	 * @return returns starting junction 
	 * @throws Exception: throws Exception if the robot doesn't have enough energy to rotate or move
	 */
	private int[] start() throws Exception{
		if(robot.isInsideRoom()){
			return startsInRoom();
		}
		else{
			return startsInHallway();
		}
	}

	@Override
	/**
	 * Implementation of Tremaux algorithm
	 */
	public boolean drive2Exit() throws OutOfEnergyException, Exception {
		// If the robot enters a room, hug the wall until it finds an exit
		// The exit will be counted as a junction
		// HashMap<junction, junction> to keep track of each junction's parent
		// Stack to keep track of moves
		// HashMap<junction, list<direction>> to keep track of the paths explored

		// Set the robot so it starts at a junction
		start();
		int[] parentJunction = robot.getCurrentPosition();
		while(!robot.isAtGoal()){
			System.out.println("(" + robot.getCurrentPosition()[0] + "," + robot.getCurrentPosition()[1] + ")");
			String position = positionToString(robot.getCurrentPosition());
			ArrayList<Integer> pathList = paths.get(position);
			// If all paths are exhausted, turn around and backtrack
			if(pathList == null){
				rotate(Turn.AROUND);
				backtrack();
			}
			else if(pathList.size() == 0){
				backtrack();
			}
			// Else, choose a random path and drive to another junction or exit
			else{
				choosePath(pathList);
				boolean exit = drive();
				if(exit == true){
					return true;
				}
				int[] currentJunction = robot.getCurrentPosition();
				String parent = positionToString(parentJunction);
				String current = positionToString(currentJunction);
				junctions.put(parent, current);
				parentJunction = currentJunction;
			}
		}
		return true;
	}
}
